

fiera_lightBox -> documentation

author -> clientcide.com (2006-9)

updated -> Saint JumanG.ru (2013/07/31)

modification -> Saint dcms-fiera.ru (2013)

Translation and description -> Saint (php-css.ru)

--------------------
Давольно функциональный скрипт "lightBox"
--------------------
Библиотека скрипта взята от разбаотчика (http://clientcide.com/wiki/cnet-libraries)
--------------------
Симпатичная галерея, позволяет открывать как отдельные штучные картинки, так и пакетно.
--------------------




<!------------------подключаем все нужно для вызова скрипта----------------------->
<script type="text/javascript" src="/sys/js/lightBox/refresh_st.js"></script>

<script type="text/javascript" src="/sys/js/lightBox/refresh_st_0.js"></script>

<link rel="stylesheet" href="/sys/js/lightBox/lightBox.css" type="text/css" media="screen" /> 

<script src="/sys/js/lightBox/lightBox.js" type="text/javascript"></script>

<script type="text/javascript">
	window.addEvent('domready',function(){
		$$('.popup_text').each(function(item){
			item.getParent('li').getElement('a').store('title',item.get('html'));
		});
	});
</script>
<!------------------подключаем все нужно для вызова скрипта----------------------->







<!--------------------------------------------------->
#	Пример использования :

Требующие обработки ссылки маркируются атрибутом js="fiera_lightBox". Если требуется организовать пролистывание нескольких изоблажений, следует указать в атрибуте некий идентификатор галереи в стиле js="fiera_lightBox[1]". Можно иметь несколько независимых галерей на странице.

Если ссылка имеет непустой атрибут title, он будет выведен как подпись во всплывающем окне. Если требуется вывести сложную подпись, с html-форматированием, следует использовать Elements Storage, сохранив требуемый код в значении title, например:


<li>
<a js="fiera_lightBox[1]" class="image" href="/images/h/4_view_h.jpg">
    <img src="/images/h/4_list_h.jpg">
</a>
    <div class="popup_text">
         <p>Некий <b>текст</b> с <i>форматированием</i>, "кавычками" и <a href="javascript:alert('!');">ссылкой</a></p>
    </div> 
 </li>
 
 
 
В скрытом блоке popup_text, помещается желаемое содержимое.
По загрузке страницы читаем его содержимое и помещаем в  хранилище элемента "a", откуда оно будет прочитано галереей
<!--------------------------------------------------->

 

 
 
 
 
 

<!--------------------------------------------------->
#	Пример использования :

 	<li>
	<a js="fiera_lightBox[1]" class="image" href="/test.png">
	<img src="test.png"></a>
	<div class="popup_text">
	Описание : bla bla bla </div>
	</li></a>
<!--------------------------------------------------->











Ну если вы уж дуб дубом ,а научится охота по примерам вот вам ->
<!--------------------------------------------------->

<ul class="demogallery">
						<li>
							<a js="fiera_lightBox" class="image" href="/1.png" title="Заголовок фото">
							<img src="/1.png" ></a>
							<p>Заголовок в title ссылки. Одиночное фото</p>
						</li>
						
						
						<li>
							<a js="fiera_lightBox[1]" class="image" href="/2.png">
							<img src="/2.png"></a>
							<p>Фото, объединенные в галеерею</p>
						</li>
						
						
						<li>
							<a js="fiera_lightBox[1]" class="image" href="/3.png">
							<img src="/3.png"></a>
							<p>Подпись с html-форматированием</p>
							<div class="popup_text">
								<p>Некий  <b>текст</b> с <i>форматированием</i>, "кавычками" и <a href="javascript:alert('!');">ссылкой</a></p>
							</div>
						</li>
						
						
						<li>
							<a js="fiera_lightBox[1]" class="image" href="/4.png">
							<img src="/4.png"></a>
							<p>Еще фото</p>
						</li>
						
						
						<li>
							<a js="fiera_lightBox[1]" class="image" href="/5.png">
							<img src="/5.png"></a>
							<p>Подпись с html-форматированием</p>
							<div class="popup_text">
								<p>Некий  <b>текст</b> с <i>форматированием</i>,
								"кавычками" и <a href="javascript:alert('!');">ссылкой</a></p>
							</div>
						</li>
</ul>

<!--------------------------------------------------->




