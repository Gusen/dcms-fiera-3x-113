<?php

if (!user_access('forum_them_edit')) {
    header('Location: '.FORUM);
    exit;
} else {
    $set['title'] = 'Жалобы на темы';
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_GET['delete'])) {
        $this_report = intval($_GET['delete']);
        if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_reports` WHERE `id` = '.$this_report), 0) != 0) {
            mysql_query('DELETE FROM `forum_reports` WHERE `id` = '.$this_report);
        }
        header('Location: '.FORUM.'/reports.html');
        exit;
    }
    $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_reports`'), 0);
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Жалоб на темы никто не оставлял.</div>
        <?
    }
    ?>
    <table class = 'post'>
        <?
        $reports = mysql_query('SELECT * FROM `forum_reports` ORDER BY `id` ASC LIMIT '.$start.', '.$set['p_str']);
        while ($report = mysql_fetch_object($reports)) {
            $who = mysql_fetch_object(mysql_query('SELECT `id`,`nick` FROM `user` WHERE `id` = '.$report->id_user));
            $theme = mysql_fetch_object(mysql_query('SELECT `id`, `id_razdel`, `name` FROM `forum_themes` WHERE `id` = '.$report->id_theme));
            $razdel = mysql_fetch_object(mysql_query('SELECT `id`, `id_forum` FROM `forum_razdels` WHERE `id` = '.$theme->id_razdel));
            $forum = mysql_fetch_object(mysql_query('SELECT `id` FROM `forum` WHERE `id` = '.$razdel->id_forum));
            ?>
            <tr>
                <td class = 'icon14'>
                    <img src = '<?= FORUM ?>/icons/report.png' alt = '' <?= ICONS ?> />
                </td>
                <td class = 'p_t'>
                    Тема: <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a> 
                    (<a href = '/info.php?id=<?= $who->id ?>'><?= $who->nick ?></a>)
                </td>
                <td class = 'icon14'>
                    <a href = '<?= FORUM ?>/reports/delete_<?= $report->id ?>'><img src = '<?= FORUM ?>/icons/delete.png' alt = '' <?= ICONS ?> /></a>
                </td>
            </tr>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    <?= output_text($report->text) ?>
                </td>
            </tr>
            <?
        }
        ?>
    </table>
    <?
    if ($k_page > 1) {
        str(FORUM.'/reports/', $k_page, $page);
    }
    ?>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM ?>'>Вернуться в форум</a></div>
    <?
}

?>